#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import OrderedDict

def configure(self):

    self.debug=False # turn on for network messaging debugging
    self.activatesounds=True # turn on to play sounds
    self.sumptuous_browser=True # turn on to use browser tabs for DQM display

    self.pubsocket_bind_address='tcp://*:5566' # address of GUI PUB socket

    # extra nodes not spawn by DAQ configuration (e.g. H4 table controller)
    self.nodes=[ 
        ('table','tcp://128.141.77.125:6999') 
    ]

    self.keepalive={} # nodes to monitor (comment to remove, never put False)
    self.keepalive['RC']=True
    #    self.keepalive['RO1']=True
    # self.keepalive['RO2']=True
    # self.keepalive['RO3']=True
    # self.keepalive['RO4']=True
    # self.keepalive['RO5']=True
    # self.keepalive['RO6']=True
    #    self.keepalive['RO2']=False
    self.keepalive['EB']=True
    # self.keepalive['DRCV1']=True
    # self.keepalive['DRCV2']=True
    #    self.keepalive['table']=True

    self.temperatureplot=None # 'http://blabla/tempplot.png' to be displayed for temperature history

    self.scripts={ # scripts linked to GUI buttons
        'sync_clocks': '../H4DAQ/scripts/syncclocks.sh',
        'free_space': None,
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_07_ECAL', #std config for h4
        #'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_07_ECAL_LOW_FREQ', #std config for h4 with digitizer frequency of 2.5 GHz
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_09_ECAL_SPIKES_LOW_FREQ', #config for ecal spikes with digitizer frequency of 2.5 GHz
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_09_ECAL_SPIKES', #config for ecal spikes
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_07_ECAL_CSI', #std config for h4

        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2017_06_MTD', #std config for h4
        # 'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --dr=pcethtb1:3:4:5:7:8: --eb=pcethtb2 --drcv=cms-h4-04,cms-h4-05 --tag=H4_2017_06_TIA_TEST --gitbranch=vfe_dev', #std config for VFEs
        #'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --gitbranch=vfe_dev --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_07_ECAL_CSI', #std config for h4
        #'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --tag=H4_2016_07_ECAL_CSI', #std config for h4

#        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --dr=pcethtb1',
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb1 --eb=pcethtb1 ',
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcminn03 --eb=pcminn03 --drcv=localhost --tag=H2_2016_06',
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --dr=pcethtb1 --eb=pcethtb2 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_06',
        #       'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_06',
        #       'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2  --drcv=cms-h4-04,cms-h4-05 --drcvrecompile --tag=H4_2016_06_LYSO',
        #       'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb2 --drcv=cms-h4-04,cms-h4-05 --drcvrecompile',
        #        'start_daemons': '../H4DAQ/scripts/startall.sh -v3 --rc=pcethtb2 --eb=pcethtb1 --dr=pcethtb1 --drcv=cms-h4-05 --drcvrecompile',
        'kill_daemons': '../H4DAQ/scripts/killall.sh --tag=H4_2016'
    }
#

    self.tableposdictionary = OrderedDict()

    # ECAL 5x5 matrix TB 2018
    #self.tableposdictionary['CEF3_CENTER']=(194.0,254.0)
    self.tableposdictionary['ZERO TABLE']=(0.0, 0.0)
    # column B
    self.tableposdictionary['VFE4_0']=(255.0, 324.0)
    self.tableposdictionary['VFE4_1']=(255.0, 301.0)
    self.tableposdictionary['VFE4_2']=(255.0, 278.0)
    self.tableposdictionary['VFE4_3']=(255.0, 255.0)
    self.tableposdictionary['VFE4_4']=(255.0, 236.0)
    # column C
    self.tableposdictionary['VFE5_4']=(231.0, 324.0)
    self.tableposdictionary['VFE5_3']=(231.0, 302.0)
    self.tableposdictionary['VFE5_2']=(231.0, 279.0)
    self.tableposdictionary['VFE5_1']=(231.0, 256.0)
    self.tableposdictionary['VFE5_0']=(231.0, 236.0)
    # column D
    self.tableposdictionary['VFE7_0']=(208.0, 324.0)
    self.tableposdictionary['VFE7_1']=(208.0, 302.0)
    self.tableposdictionary['VFE7_2']=(208.0, 280.0)
    self.tableposdictionary['VFE7_3']=(208.0, 255.0)
    self.tableposdictionary['VFE7_4']=(208.0, 236.0)
    # Intermidiate position around VFE5_2
    self.tableposdictionary['VFE5_2_sx']=(242.5, 279.0)
    self.tableposdictionary['VFE5_2_dx']=(219.5, 279.0)
    self.tableposdictionary['VFE5_2_up']=(231.0, 290.5)
    self.tableposdictionary['VFE5_2_dn']=(231.0, 267.5)

##
    # for i,j in otherxtals.iteritems():
    #     self.tableposdictionary[i]=(self.tableposdictionary['CEF3_CENTER'][0]+j[0],self.tableposdictionary['CEF3_CENTER'][1]-j[1])
    #     #        self.tableposdictionary[i]=(self.tableposdictionary['CAMERONE_CENTER'][0]+j[0],self.tableposdictionary['CAMERONE_CENTER'][1]-j[1])
