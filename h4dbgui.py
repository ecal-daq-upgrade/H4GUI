from pymongo import MongoClient
import copy
import socket

class DBGUIInterface:

    def __init__(self):

        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client['test']
        self.SConfigs = self.db['services_configs']
        self.RunKeys = self.db['run_keys']
        self.AppConfigs = self.db['apps_configs']
        # Line to test the connection to the Mongo DB
        # print collection.find_one()

    def getSConfigsTagsList(self):
        return self.SConfigs.distinct("tag")

    def getRunKeysTagsList(self):
        return self.RunKeys.distinct("tag")

    def getSConfigsVersList(self,mytag):
        firstList = self.SConfigs.find({"tag": mytag}, {"v":1,"_id":0})
        sortedList = []
        for ver in firstList:
            sortedList.append(ver["v"])
        sortedList.sort()
        reversList = sortedList[::-1]
        return reversList

    def getRunKeysVersList(self,mytag):
        firstList = self.RunKeys.find({"tag": mytag}, {"v":1,"_id":0})
        sortedList = []
        for ver in firstList:
            sortedList.append(ver["v"])
        sortedList.sort()
        reversList = sortedList[::-1]
        return reversList

    def getRKmaxTag(self,mytag):
        for post in self.RunKeys.aggregate([{ "$sort": { "v": -1 } },{ "$match": { "tag": mytag }},{ "$group": { "_id": "$tag", "v": { "$first": "$v" } } }]):
            singleRow = post
        return self.RunKeys.find_one({"tag": mytag, "v": singleRow["v"]})

    def getRKbyTagbyVer(self,mytag,myver):
        return self.RunKeys.find_one({"tag": mytag, "v": int(myver)})

    def getSCmaxTag(self,mytag):
        for post in self.SConfigs.aggregate([{ "$sort": { "v": -1 } },{ "$match": { "tag": mytag }},{ "$group": { "_id": "$tag", "v": { "$first": "$v" } } }]):
            singleRow = post
        return self.SConfigs.find_one({"tag": mytag, "v": singleRow["v"]})

    def getSCbyTagbyVer(self,mytag,myver):
        return self.SConfigs.find_one({"tag": mytag, "v": int(myver)})

    def getAppConfigByVersion(self,appname,appversion):
        return self.AppConfigs.find_one({"appl": appname, "v": appversion})

    # Create the network map from the configuration of the services
    def createNetworkMap(self, sconf):
        services = copy.deepcopy(sconf)
        list_of_machines = {}
        for service in services:
            list_of_machines[service["hostname"]] = {'data':0,'status':0,'cmd':0}

        # Inserting services and local ports in network map
        for service in services:
            service["localNet"]=copy.deepcopy(self.getLocalPorts(list_of_machines,service["hostname"]))

        for service in services:
            if service["appname"] == "RC":
                rc_i = services.index(service)
            elif service["appname"] == "EB":
                eb_i = services.index(service)
            service['externalNet'] = []

        # Adding external ports in the network map
        for service in services:
            if service["appname"] == "DRCV":
                service['externalNet'].append(services[eb_i]['hostname']+":"+str(services[eb_i]['localNet']['data']))
            if service["appname"] != "RC":
                services[rc_i]['externalNet'].append(service["hostname"]+":"+str(service["localNet"]['cmd']))
            if service["appname"] != "EB" and service["appname"] != "DRCV":
                services[eb_i]['externalNet'].append(service["hostname"]+":"+str(service["localNet"]['data']))
            if service["appname"] != "RC" and service["appname"] != "EB":
                service['externalNet'].append(services[rc_i]['hostname']+":"+str(services[rc_i]['localNet']['cmd']))
                service['externalNet'].append(socket.getfqdn()+":5567") # Emergency CML

        # Adding standard ports
        services[rc_i]['externalNet'].append(socket.getfqdn()+":5566") # GUI
        services[rc_i]['externalNet'].append(socket.getfqdn()+":5567") # Emergency CML
        services[eb_i]['externalNet'].append(services[rc_i]['hostname']+":"+str(services[rc_i]['localNet']['cmd'])) # GUI
        services[eb_i]['externalNet'].append(socket.getfqdn()+":5567") # Emergency CML

        print("##########################")
        for service in services:
            print(service["appname"],service["hostname"],service["localNet"],service["externalNet"])

        return services

    def getLocalPorts(self,machines,hostname):
        if machines[hostname]['data'] == 0:
             machines[hostname] = {'data':7000,'status':7002,'cmd':7004}
             return machines[hostname]
        else:
            machines[hostname]['data'] = machines[hostname]['data']+10
            machines[hostname]['status'] = machines[hostname]['status']+10
            machines[hostname]['cmd'] = machines[hostname]['cmd']+10
            return machines[hostname]
