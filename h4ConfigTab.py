import pygtk
pygtk.require('2.0')
import gtk
import gobject
import pygst
import gst
import subprocess
from h4dbgui import *
import json
import xml.dom.minidom
from bs4 import BeautifulSoup as Soup
import socket
import time

class ConfigTab:

    def destroy(self,wid,nb,tablist,*args):
        nb.remove_page(nb.get_current_page())
        if nb.get_n_pages()==2: # tab 0,1 are webcam viewers
            ConfigTab(nb,tablist)
        tablist.remove(self)
        del self

    def __init__(self,nb,tablist):

        # Dynamic scripts
        self.launch_script = ""

        self.base_path = "/tmp/H4/"
        self.tag_path = ""
        # List of services extracted from Services Configuration table
        self.machines_services_db = []
        # List of services from Service Configuration table && selected DR in GUI
        self.machines_services = []
        self.services_config_tag = ""

        # Connection to Mongo DB
        self.mongodb = DBGUIInterface()

        # Error flag
        self.error_flag = False

        # Boxes
        self.vbox_general = gtk.VBox()
        self.hbox_1 = gtk.HBox()
        self.hbox_1a = gtk.HBox()
        self.hbox_1b = gtk.HBox()
        self.hbox_2 = gtk.HBox()
        self.hbox_3 = gtk.HBox()

        # DR Frame
        self.DR_frame = gtk.Frame("DR List")
        self.DR_bbox = gtk.HButtonBox()
        self.DR_bbox.set_border_width(5)
        self.DR_frame.add(self.DR_bbox)
        self.DR_bbox.set_layout(gtk.BUTTONBOX_START)
        self.exbutton = gtk.Button(stock=gtk.STOCK_OK)
        self.DR_buttonArray = []

        # Small widgets
        self.refresh_label = gtk.Label("Refresh all configs from DB:")
        self.refresh_button = gtk.Button('Refresh','gtk-refresh')
        self.loadold_label = gtk.Label("Load old config on DB:")
        self.loadold_button = gtk.Button('Load old')

        self.sconfigs_combobox = gtk.combo_box_new_text()
        self.sconfigs_combobox.connect('changed', self.changed_sconfigs_cb)
        self.sconfigs_combobox.set_title("sconfigtagbox")
        self.sc_ver_combobox = gtk.combo_box_new_text()
        self.sc_ver_combobox.set_title("sconfigverbox")
        self.sc_ver_combobox.connect('changed', self.changed_sc_ver_cb)
        self.sconfigs_label = gtk.Label("Service Configurations tags:")

        self.runkeys_combobox = gtk.combo_box_new_text()
        self.runkeys_combobox.connect('changed', self.changed_runkeys_cb)
        self.rk_ver_combobox = gtk.combo_box_new_text()
        self.runkeys_label = gtk.Label("Run keys tags:")

        self.dbgui_link = gtk.LinkButton("http://localhost:3000/", "Open Local DBGui")
 
        # create button styles
        dummybutton = gtk.ToggleButton("Dummy")
        # dummybutton.set_active(True)
        # colormap = dummybutton.get_colormap()
        # color_red = colormap.alloc_color("red")
        # color_green = colormap.alloc_color("green")
        # color_yellow = colormap.alloc_color("yellow")
        color_green = gtk.gdk.Color("#99ff33")
        color_red = gtk.gdk.Color("#EF5350")
        self.stylecolored = dummybutton.get_style().copy()
        self.stylecolored.bg[gtk.STATE_ACTIVE] = color_green
        self.stylecolored.bg[gtk.STATE_NORMAL] = color_red
        # self.stylecolored.bg[gtk.STATE_SELECTED] = color_yellow
 
        # Actions
        self.refresh_button.connect('clicked',self.getConfigsFromDB)
        self.loadold_button.connect('clicked',self.findOldConfig)

        # Building GUI
        self.hbox_1a.pack_start(self.refresh_label,False,False)
        self.hbox_1a.pack_start(self.refresh_button,False,False)
        self.hbox_1a.pack_start(self.loadold_label,False,False)
        self.hbox_1a.pack_start(self.loadold_button,False,False)
        self.hbox_1b.pack_start(self.dbgui_link,False,False)
        self.hbox_2.pack_start(self.sconfigs_label,False,False)
        self.hbox_2.pack_start(self.sconfigs_combobox,False,False)
        self.hbox_2.pack_start(self.sc_ver_combobox,False,False)
        self.hbox_2.pack_start(self.runkeys_label,False,False)
        self.hbox_2.pack_start(self.runkeys_combobox,False,False)
        self.hbox_2.pack_start(self.rk_ver_combobox,False,False)
        self.hbox_3.pack_start(self.DR_frame,True,True)
        self.hbox_1.pack_start(self.hbox_1a,True,True)
        self.hbox_1.pack_start(self.hbox_1b,False,False)
        self.vbox_general.pack_start(self.hbox_1,False,False)
        self.vbox_general.pack_start(self.hbox_2,False,False)
        self.vbox_general.pack_start(self.hbox_3,False,False)


        # Inserting tab in main gui
        self.lab = gtk.Label('Configurations')
        nb.append_page(self.vbox_general,self.lab)
        nb.show_all()
        nb.set_current_page(-1)
        tablist.append(self)

        # Check existence of a previous configuration and load it
        self.getConfigsFromDB(self.refresh_button)


    def getConfigsFromDB(self,theButton):

        self.runkeys_combobox.handler_block_by_func(self.changed_runkeys_cb)
        self.sconfigs_combobox.handler_block_by_func(self.changed_sconfigs_cb)

        self.sconfigs_combobox.get_model().clear()
        self.runkeys_combobox.get_model().clear()
        self.sc_ver_combobox.get_model().clear()
        self.rk_ver_combobox.get_model().clear()

        for i in self.DR_bbox.get_children():
            self.DR_bbox.remove(i)
        del self.DR_buttonArray[:]

        try:
            print("Getting the list of Services Configurations Tags...")
            sconfigsTagsList = self.mongodb.getSConfigsTagsList()
            print("Getting the list of Run Keys Tags...")
            runkeysTagsList = self.mongodb.getRunKeysTagsList()
            for tag in sconfigsTagsList:
                self.sconfigs_combobox.append_text(tag)
            for tag in runkeysTagsList:
                self.runkeys_combobox.append_text(tag)
            self.runkeys_combobox.handler_unblock_by_func(self.changed_runkeys_cb)
            self.sconfigs_combobox.handler_unblock_by_func(self.changed_sconfigs_cb)

        except ValueError:
            print("Error filling the dropdown menu.")
            self.error_flag = True

    def changed_sconfigs_cb(self, combobox):
        tag = self.sconfigs_combobox.get_active_text()
        print("Getting the list of Services Configurations versions for tag:",tag)
        sconfigsVersList = self.mongodb.getSConfigsVersList(tag)
        self.sc_ver_combobox.get_model().clear()
        for ver in sconfigsVersList:
            self.sc_ver_combobox.append_text(str(ver))
        self.sc_ver_combobox.set_active(0)

    def changed_sc_ver_cb(self, combobox):
        for i in self.DR_bbox.get_children():
            self.DR_bbox.remove(i)
        del self.DR_buttonArray[:]
        # print "button array: ", self.DR_buttonArray
        # Create list of DRs in the GUI
        self.services_config_tag = self.sconfigs_combobox.get_active_text()
        services_config_ver = self.sc_ver_combobox.get_active_text()
        print("Services Configuration tag: ",self.services_config_tag," version: ",services_config_ver)
        services_config = self.mongodb.getSCbyTagbyVer(self.services_config_tag,services_config_ver)
        self.machines_services_db = json.loads(services_config["machines"])
        self.machines_services = copy.deepcopy(self.machines_services_db)
        print self.machines_services
        for service in self.machines_services["services"]:
            # if service["appname"] == "DR":
            if "DR_" in service["appname"]:
                print "service DR_: button array -> ", service["appname"]
                button_label = service["hostname"]
                if "crateId" in service:
                    button_label = button_label + "-"
                    button_label = button_label + str(service["crateId"])
                self.DR_buttonArray.append(gtk.ToggleButton(button_label,'gtk-refresh'))
        for button in self.DR_buttonArray:
            self.DR_bbox.add(button)
            button.set_active(True)
            button.set_style(self.stylecolored)
            button.show()

    def changed_runkeys_cb(self, combobox):
         tag = self.runkeys_combobox.get_active_text()
         print("Getting the list of Run Keys versions for tag:",tag)
         runkeysVersList = self.mongodb.getRunKeysVersList(tag)
         self.rk_ver_combobox.get_model().clear()
         for ver in runkeysVersList:
             self.rk_ver_combobox.append_text(str(ver))
         self.rk_ver_combobox.set_active(0)

    def prepareConfiguration(self):
        try:
            print("Preparing the XML files from DB configurations...")
            run_key_tag = self.runkeys_combobox.get_active_text()
            run_key_ver = self.rk_ver_combobox.get_active_text()
            print("Run key tag: ",run_key_tag," version: ",run_key_ver)
            run_key = self.mongodb.getRKbyTagbyVer(run_key_tag,run_key_ver)
            new_services_config = []
            # Remove DRs deactivated from GUI
            print "services in DB: ", self.machines_services_db
            for service in self.machines_services_db["services"]:
                # if service["appname"] == "DR":
                if "DR_" in service["appname"]:
                    print "DR_ :" + service["appname"]
                    createdName = service["hostname"]
                    print "created name: ", createdName
                    if "crateId" in service:
                        createdName = createdName + "-"
                        createdName = createdName + str(service["crateId"])
                        print " created name - edit: ", createdName
                    for button in self.DR_buttonArray:
                        print "button: ", button.get_label(), button.get_active()
                        if button.get_label() == createdName and button.get_active():
                            print "Active in DR_", service
                            new_services_config.append(service)
                else:
                    new_services_config.append(service)
            print "new services: ", new_services_config
            print "machines services: ", self.machines_services
            self.machines_services["services"] = new_services_config

            # create Folders + empty it
            return_code = self.createConfigFolder()
            if return_code:
                raise Exception("Error creating configuration folder")
            print("Temp folder for configuration files created!")

            # create files with name from list of services and fill them
            return_code = self.createConfigFiles(run_key)
            if return_code:
                raise Exception("Error creating configuration files")
            print("Configuration files created in temp folder!")

            # distribute XML folder all over the machines
            # move only required config files or the full folder?
            return_code = self.distributeConfigFiles()
            if return_code:
                raise Exception("Error moving configuration files on other machines")
            print("Configuration files copied on the other machines!")

            # create string for startall script
            #self.launch_script = self.createScriptString()

        except Exception as e:
            print("Error preparing the configuration:")
            print(str(e))
            self.error_flag = True


    def createConfigFolder(self):
        self.tag_path = self.base_path + self.services_config_tag + '_' + str(int(time.time()))
        # self.tag_path += self.base_path + self.services_config_tag + '_' + str(int(time.time()))
        print("TAG PATH: ", self.tag_path)
        return_code = subprocess.call("mkdir -p "+self.tag_path, shell=True)
        if return_code:
            return_code = subprocess.call("rm -rf "+self.tag_path, shell=True)
            return_code = subprocess.call("mkdir -p "+self.tag_path, shell=True)
        return return_code

    def createConfigFiles(self, run_key):

        return_code = 0
        # check if EB and RC exist
        RC_present = False
        EB_present = False
        for service in self.machines_services["services"]:
            if service["appname"] == "RC":
                RC_present = True
            if service["appname"] == "EB":
                EB_present = True

        if not RC_present or not EB_present:
            print("Missing EB or RC in the list of services. Something wrong...")
            return 1


        # Creating networks
        # extended_services contains also the network connections details for each application
        extended_services = self.mongodb.createNetworkMap(self.machines_services["services"])
        self.machines_services["services"] = extended_services

        DRs_number_for_RC = 0
        events_number_for_EB = 1
        for service in extended_services:
            # if service["appname"] == "DR":
            if "DR_" in service["appname"]:
                DRs_number_for_RC = DRs_number_for_RC + 1
                events_number_for_EB = events_number_for_EB + 1

        for service in extended_services:
            file_name = "config_" + service["hostname"] + "_" + service["appname"] + "_" + str(service["localNet"]["status"]) + ".xml"
            file_path = self.tag_path+"/"+file_name
            # Temporary trick. The host name in the configuration is used both to
            # create the file name and the ConnectTo tags in the xml
            # But the file name doesn't use .cern.ch while ConnectTo requires it.
            new_file_path = file_path.replace(".cern.ch","")

            return_code += subprocess.call("touch "+new_file_path, shell=True)
            if return_code:
                break
            app_version = run_key[service["appname"].lower()]
            app_config = self.mongodb.getAppConfigByVersion(service["appname"],app_version)

            soup = Soup(app_config["conf"], "xml")
            for key,value in service.iteritems():
                if key != "appname" or key != "hostname" or key != "localNet" or key != "externalNet":
                    for tag in soup.findAll(key):
                        tag.string = str(value)
                if key == "initcmd":
                    service['initcmd'] = value
            if service["appname"] == "RC":
                for tag in soup.findAll("waitForDR"):
                    tag.string = str(DRs_number_for_RC)
            if service["appname"] == "EB":
                for tag in soup.findAll("recvEvent"):
                    tag.string = str(events_number_for_EB)
            if service["appname"] == "DR_VFE":
                if "crateId" not in service.keys():
                    print "Invalid configuration: no crateId specified for VFE"
                    return 1
                for tag in soup.findAll("Device"):
                    tag.string = "vice.udp."+str(service["crateId"])
                for tag in soup.findAll("dumpDirName"):
                    tag.string = "/tmp/raw/dr"+str(service["crateId"])+"/"
                for board in soup.findAll("board"):
                    if board.type.string == "VFE_ADAPTER":
                        board.ID.string = str(service["crateId"])

            # Adding the network parameters to the xml
            for tag in soup.findAll("Network"):
                for localport in "data", "status", "cmd":
                    new_tag = soup.new_tag("ListenPort")
                    new_tag.append(str(service["localNet"][localport]))
                    print(new_tag)
                    tag.append(new_tag)
                for externalport in service["externalNet"]:
                    new_tag = soup.new_tag("ConnectTo")
                    new_tag.append(str(externalport))
                    tag.append(new_tag)
            f = open(new_file_path, 'w')
            #app_config_xml_pretty = app_config["conf"]
            #f.write(app_config_xml_pretty)
            #f.write(soup.prettify())
            f.write(str(soup))
            f.close()

        # Create description file
        file_path = self.tag_path+"/config_version.jsn"
        config_version = {}
        config_version["services_tag"] = self.services_config_tag
        config_version["services_ver"] = self.sc_ver_combobox.get_active_text()
        config_version["runkey_tag"] = self.runkeys_combobox.get_active_text()
        config_version["runkey_ver"] = self.rk_ver_combobox.get_active_text()
        with open(file_path, 'w') as fp:
            json.dump(config_version, fp)
        return return_code

    def distributeConfigFiles(self):

        gui_hostname = socket.gethostname()

        all_machines = []
        for service in self.machines_services["services"]:
            all_machines.append(service["hostname"])
        list_of_machines = set(all_machines)
        return_code = 0
        for machine in list_of_machines:
            if gui_hostname in machine:
                continue
            subprocess.call("ssh cmsdaq@"+machine+" mkdir "+self.base_path, shell=True)
            copy_string = "scp -r "+self.tag_path+" cmsdaq@"+machine+":"+self.base_path
            return_code += subprocess.call(copy_string, shell=True)
            subprocess.call("ssh cmsdaq@"+machine+" rm "+self.base_path+"latest", shell=True)
            time.sleep(1)
            return_code += subprocess.call("ssh cmsdaq@"+machine+" ln -s "+self.tag_path+" "+self.base_path+"latest", shell=True)
        return return_code

    def deleteAllConfigFiles(self):

        gui_hostname = socket.gethostname()

        all_machines = []
        for service in self.machines_services["services"]:
            all_machines.append(service["hostname"])
        list_of_machines = set(all_machines)
        return_code = 0
        for machine in list_of_machines:
            if gui_hostname in machine:
                copy_string = "rm -rf "+self.tag_path
                return_code += subprocess.call(copy_string, shell=True)
            else:
                copy_string = "ssh -t "+" cmsdaq@"+machine+" rm -rf "+self.tag_path
                return_code += subprocess.call(copy_string, shell=True)
        return return_code

    # It looks in the local folder where the xml are created and launch them depending on:
    # process type, machine name and possible special launch commands
    # also creates a symlink to the latest DAQ configuration
    def launchAllApplications(self):

        p = subprocess.Popen(('ls '+self.tag_path+'/*.xml'), shell=True, stdout=subprocess.PIPE, close_fds=True)
        files_list = [path.rstrip('\n') for path in p.stdout.readlines()];
        # Looking for RC file and putting it as first in the list
        for ind, file_name in enumerate(files_list):
            if files_list[ind].find("_RC_") != -1:
                RC_ind = ind
                RC_file = files_list[ind]
        del(files_list[RC_ind])
        files_list.insert(0,RC_file)

        for filename in files_list:
            print(filename)
            onlyname = filename[filename.rfind("/")+1:]
            # Get machine name from file
            machinename = onlyname.split("_")[1];
            machinename = machinename + ".cern.ch"
            apptype = onlyname.split("_")[2];
            appport = onlyname.split("_")[3];
            appport = appport[:-4]

            # If the service has the initcmd defined we use that one instead of /bin/blabla
            initCommand = self.getInitCmd(apptype,machinename,appport)

            # Creating launch command
            command = ""
            command += "ssh cmsdaq@"+machinename+" '"

            if initCommand != "":
                command += initCommand
            # elif apptype == "DR":
            elif apptype == "DR":
                command += "/home/cmsdaq/DAQ/H4DAQ/bin/datareadout"
            elif apptype == "RC":
                command += "/home/cmsdaq/DAQ/H4DAQ/bin/runcontrol"
            elif apptype == "EB":
                command += "/home/cmsdaq/DAQ/H4DAQ/bin/eventbuilder"
            elif apptype == "DRCV":
                command += "/home/cmsdaq/DAQ/H4DAQ/bin/datareceiver"

            command += " -d -c "+self.tag_path+"/"+onlyname
            command += " -v 3 -l "+self.tag_path+"/"+onlyname[:-4].replace('config', 'log')+".log"
            command += " > /tmp/log_h4daq_start_"+apptype+"_"+onlyname[7:-4]+"_"+str(int(time.time()))+".log"
            command += "' 2>&1 | tee  /tmp/log_h4daq_update_"+apptype+"_"+onlyname[7:-4]+"_"+str(int(time.time()))+".log"

            subprocess.call("rm /tmp/H4/latest", shell=True)
            time.sleep(1)
            subprocess.call("ln -s "+self.tag_path+" /tmp/H4/latest", shell=True)
            print(command)  
            subprocess.call(command, shell=True)

    def getInitCmd(self,apptype,machinename,appport):
        for service in self.machines_services["services"]:
            if apptype == service["appname"]:
                if str(appport) == str(service["localNet"]["status"]):
                    if machinename+".cern.ch" == service["hostname"]:
                        if hasattr(service, 'initcmd'):
                            return service.property
                        else:
                            return ""
	return ""

    def killAllApplications(self):
        pathtemp = self.tag_path+"/*.xml"
        # print pathtemp
        p = subprocess.Popen(('ls '+ pathtemp), shell=True, stdout=subprocess.PIPE, close_fds=True)
        # for path in p.stdout.readlines():
        #     print path
        files_list = [path.rstrip('\n') for path in p.stdout.readlines()];
        for filename in files_list:
            # Get machine name from file
            # print "Filename: ",filename
            filename = filename[filename.rfind("/"):]
            # print "Filenamee: ",filename            
            machinename = filename.split("_")[1];
            machinename = machinename + ".cern.ch"
            # print machinename
            apptype = filename.split("_")[2];
            appport = filename.split("_")[3];
            appport = appport[:-4]

            p = subprocess.Popen(('ssh', "cmsdaq@"+machinename, " pgrep -f "+ filename), shell=False, stdout=subprocess.PIPE, close_fds=True)
            # print p.stdout
            for pid in p.stdout.readlines():
                pid = pid[:-1]
                # print "pid: ",pid
                p = subprocess.Popen(('ssh', "cmsdaq@"+machinename, " kill "+pid), shell=False, stdout=subprocess.PIPE, close_fds=True)
        subprocess.call("rm /tmp/H4/latest", shell=True)

    def findOldConfig(self,theButton):
        p = subprocess.Popen(('ls', self.base_path), shell=False, stdout=subprocess.PIPE, close_fds=True)
        folder_list = [path.rstrip('\n') for path in p.stdout.readlines()];
        if len(folder_list) == 0:
            print("No old configs found in tmp... create a new one.")
        else:
            self.tag_path = self.base_path+folder_list[0]
            print("Found old config in tmp: ", self.tag_path)
            with open(self.tag_path+"/config_version.jsn", 'r') as fp:
                data = json.load(fp)
            services_config_tag_tmp = data["services_tag"]
            services_config_ver_tmp = data["services_ver"]
            run_key_tag_tmp = data["runkey_tag"]
            run_key_ver_tmp = data["runkey_ver"]
            model = self.sconfigs_combobox.get_model()
            for i, k in enumerate(model):
                itr = model.get_iter(i)
                title = model.get_value(itr,0)
                if title == services_config_tag_tmp:
                    self.sconfigs_combobox.set_active_iter(itr)
            model = self.sc_ver_combobox.get_model()
            for i, k in enumerate(model):
                itr = model.get_iter(i)
                title = model.get_value(itr,0)
                if title == services_config_ver_tmp:
                    self.sc_ver_combobox.set_active_iter(itr)
            model = self.runkeys_combobox.get_model()
            for i, k in enumerate(model):
                itr = model.get_iter(i)
                title = model.get_value(itr,0)
                if title == run_key_tag_tmp:
                    self.runkeys_combobox.set_active_iter(itr)
            model = self.rk_ver_combobox.get_model()
            for i, k in enumerate(model):
                itr = model.get_iter(i)
                title = model.get_value(itr,0)
                if title == run_key_ver_tmp:
                    self.rk_ver_combobox.set_active_iter(itr)

    def prepareNetwork(self):
        #('RC','tcp://pcethtb2.cern.ch:6002')
        new_nodes = []
        for service in self.machines_services["services"]:
            appNameString=service["appname"]
            if "crateId" in service:
                appNameString = appNameString + str(service["crateId"])
            address = 'tcp://'+service["hostname"]+":"+str(service["localNet"]["status"])
            new_nodes.append((appNameString,address))
        #new_nodes.append(('table','tcp://128.141.77.125:6999'))
        return new_nodes

    def checkSelection(self):
        # print "checkSelection: ",self.runkeys_combobox.get_active_text()
        # print "checkSelection: ",self.sconfigs_combobox.get_active_text()
        # print "checkSelection: ",self.rk_ver_combobox.get_active_text()
        # print "checkSelection: ",self.sc_ver_combobox.get_active_text()
        if self.runkeys_combobox.get_active_text() == None:
            return False
        if self.sconfigs_combobox.get_active_text() == None:
           return False
        if self.rk_ver_combobox.get_active_text() == None:
            return False
        if self.sc_ver_combobox.get_active_text() == None:
            return False
        
        return True
